
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Person {
  String? firstName;
  String? lastName;
  String? email;
  Person({this.firstName, this.lastName, this.email});
  factory Person.fromJson(Map<String, dynamic> json)=> _$PersonFromJson(json);
  Map<String, dynamic> toJson()=> _$PersonToJson(this);
}