import 'package:flutter/material.dart';
import 'package:hive/hive.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hive Data base "),
      ),
      body: Column(
        children: [

        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onPress,
        child: const Icon(Icons.add),
      ),
    );
  }

  void _onPress() async {
    var box = await Hive.openBox("customer");
    box.put("name", "Machhindra Neupane");
    box.put("details", {

    });
    print(box.get("name"));
  }

}

