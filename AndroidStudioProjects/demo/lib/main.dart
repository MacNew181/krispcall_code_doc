import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
final nameProvider = Provider<String>((ref) => "Machhindra Neupane");

final nameProviders = Provider<String>((ref) => "Mahendra Neupane");
void main() {
  runApp(const ProviderScope(child:MyApp()));
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:  Main(),
    );
  }
}
class Main extends ConsumerStatefulWidget {
  const Main({Key? key}) : super(key: key);

  @override
  _MainState createState() => _MainState();
}

class _MainState extends ConsumerState<Main> {
  @override
  Widget build(BuildContext context) {
    final name = ref.watch(nameProvider);
    return  Scaffold(body: Center(child: Text(name)));
  }
}
